<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReconstructInvoiceTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::dropIfExists('invoice_order');
        
        Schema::create('invoice_lines', function($table) {
			$table->increments('id');
            
            $table->integer('invoice_id');
            
            $table->string('name');
            $table->double('unit_price');
            $table->double('vat_percentage');
            
            $table->float('amount');
            $table->double('total_price');
            $table->double('total_vat');
            
            $table->timestamps();
        });
	}
    
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('invoice_lines');
        
        $invoiceOrderTable = new CreateInvoiceOrder;
        $invoiceOrderTable->up();
	}
    
}
