<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceSubjectDescription extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('invoice', function($table) {
            $table->string('subject')->after('client_id');
            $table->text('description')->after('subject');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('invoice', function($table) {
            $table->dropColumn('subject');
            $table->dropColumn('description');
        });
	}

}
