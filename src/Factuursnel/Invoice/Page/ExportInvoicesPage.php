<?php namespace Factuursnel\Invoice\Page;

use Clearweb\Clearwebapps\Layout\FullWidthLayout;

use Factuursnel\Invoice\Widget\ExportInvoicesWidget;

use Factuursnel\Client\Client;

use Clearweb\Clearwebapps\Page\Page as BasePage;

class ExportInvoicesPage extends BasePage
{
    public function __construct() {
        $this->setSlug(trans('invoice::nav.export_invoices'));
    }

    public function init() {
		$this->setLayout(new FullWidthLayout);
        
        $client = Client::find($this->getParameter('client_id', 0));
        
        
        if (empty($client)) {
			$this->setTitle(ucfirst(trans('invoice::export.export_invoices')));
		} else {
            $this->setTitle(ucfirst(trans('invoice::export.export_invoices_for', array('client'=>$client->name))));
        }

                    
        $this->addWidgetLinear(new ExportInvoicesWidget, 'content');
        
        return parent::init();
    }
}