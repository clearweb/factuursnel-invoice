<?php namespace Factuursnel\Invoice\Page;

use Clearweb\Clearwebapps\Layout\FullWidthLayout;

use Factuursnel\Invoice\Widget\MakeInvoiceForm;
use Factuursnel\Invoice\Widget\DraftInvoiceLineForm;
use Factuursnel\Invoice\Widget\DraftInvoiceLineList;

use Factuursnel\Client\Client;

use Clearweb\Clearwebapps\Page\Page as BasePage;

class MakeInvoicePage extends BasePage
{
    public function __construct() {
        $this->setSlug(trans('nav.make_invoice'));
    }

    public function init() {
		$this->setLayout(new FullWidthLayout);
        
        $client = Client::find($this->getParameter('client_id', 0));
        
        
		$this->addWidgetLinear(new DraftInvoiceLineForm, 'content')
             ->addWidgetLinear(new DraftInvoiceLineList, 'content')
			;        
        
        if (empty($client)) {
			$this->setTitle('...');
		} else {
            $this->setTitle(ucfirst(trans('app.make_invoice_for', array('client'=>$client->name))));
            
            $this->addWidgetLinear(new MakeInvoiceForm, 'content');
        }
        
        return parent::init();
    }
}