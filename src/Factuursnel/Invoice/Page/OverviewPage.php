<?php namespace Factuursnel\Invoice\Page;

use Clearweb\Clearwebapps\Layout\FullWidthLayout;

use Factuursnel\Client\Client;

use Factuursnel\Invoice\Widget\ListWidget;

use Order\Page\MakeInvoiceFromOrderPage;

use Clearweb\Clearworks\Widget\ContainerWidget;
use Clearweb\Clearworks\Action\ActionAnchor;

use Clearworks;
use Event;

use Clearweb\Clearwebapps\Page\Page as BasePage;

class OverviewPage extends BasePage
{
	public function __construct() {
		$this->setSlug(trans('nav.invoices'));
	}
	
	public function init() {
		$this->setLayout(new FullWidthLayout);
        
        $client = Client::find($this->getParameter('client_id', 0));
        
        $invoiceList = new ListWidget;
        
        if (empty($client)) {
			$this->setTitle(ucfirst(trans_choice('app.invoice', 2)));
            $this->addAction(
							 'export-invoices',
							 with(new ActionAnchor)
							 ->setUrl(Clearworks::getPageUrl(new ExportInvoicesPage))
							 ->setTitle(trans('invoice::export.export_invoices'))
							 ->addClass('new-button')
            );
		} else {
            $this->setTitle(ucfirst(trans('app.invoices_for', array('client'=>$client->name))));
			
			$this->addAction(
							 'make-invoice',
							 with(new ActionAnchor)
							 ->setUrl(Clearworks::getPageUrl(new MakeInvoicePage, array('client_id'=>$client->id)))
							 ->setTitle(trans('app.make_invoice'))
							 ->addClass('new-button')
                             );
            
            $this->addAction(
							 'export-invoices',
							 with(new ActionAnchor)
							 ->setUrl(Clearworks::getPageUrl(new ExportInvoicesPage, array('client_id'=>$client->id)))
							 ->setTitle(trans('invoice::export.export_invoices'))
							 ->addClass('new-button')
							)
				;
            
            Event::fire('invoice.client-actions', ['page'=>$this]);
        }
        
        $this->addWidgetLinear($invoiceList, 'content');
        
		return parent::init();
	}
}