<?php namespace Factuursnel\Invoice\Page;

use Clearweb\Clearwebapps\Layout\FullWidthLayout;

use Factuursnel\Invoice\Widget\InvoiceLineListWidget;
use Factuursnel\Invoice\Widget\InvoiceHeader;
use Factuursnel\Invoice\Widget\InvoiceFooter;

use Factuursnel\Invoice\Invoice;

use Clearweb\Clearworks\Action\ScriptActionButton;
use Clearweb\Clearworks\Widget\ContainerWidget;


use Factuursnel\Base\Action\PrintWindow;

use Clearweb\Clearwebapps\Page\Page as BasePage;

class ViewPage extends BasePage
{
    public function __construct() {
		$this->setSlug(trans('nav.view_invoice'));
	}
	
	public function init() {
		$this->setLayout(new FullWidthLayout);
        
        $invoice = Invoice::find($this->getParameter('invoice_id', 0));
        
        $this->setTitle(trans('app.invoice_with_nr', array('invoice'=>$invoice->getNumber())));
        
        $this->addWidgetLinear(new InvoiceHeader, 'content')
            ->addWidgetLinear(new InvoiceLineListWidget, 'content')
            ->addWidgetLinear(new InvoiceFooter, 'content')
            ;
        
        $printButtonWidget = new ContainerWidget;
		$printButtonWidget->init();
        $printButtonWidget->addStyle('invoices/css/order_view.css');
		$printButtonWidget->getContainer()
			->addViewable(
							with(new ScriptActionButton)
							->setScriptAction(new PrintWindow)
							->setTitle(trans('app.print'))
							->addClass('print-button')
							->addClass('button')
						)
            ->addClass('print-widget')
		;

		$this->addWidgetLinear($printButtonWidget, 'content');
        
        parent::init();
        
        return $this;
    }
}
