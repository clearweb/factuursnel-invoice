<?php namespace Factuursnel\Invoice\Widget;

use Clearweb\Clearwebapps\Action\AjaxConfirmAnchor;

use Clearweb\Clearworks\Communication\ReloadCurrentWidget;

use Factuursnel\Invoice\Invoice;

use Clearworks;

class ChangeStatusLink extends AjaxConfirmAnchor
{
	public function execute()
    {
		$invoice = Invoice::find($this->getParameter('id', 0));
        
        if ($invoice->getTotalPrice() > 0) {
        
            if ($invoice->getStatus() == Invoice::STATUS_OPEN) {
                $this->setTitle(trans('app.mark_as_paid'));
                $this->setUrl(Clearworks::getActionUrl(new MarkAsClosed, array('invoice_id'=>$invoice->id)));
                $this->setConfirmTitle(trans('app.mark_as_paid'));
                $this->setConfirmMessage(trans('app.mark_as_paid_message', array('invoice'=>$invoice->name)));
            } elseif($invoice->getStatus() == Invoice::STATUS_PAID) {
                $this->setTitle(trans('app.mark_as_open'));
                $this->setUrl(Clearworks::getActionUrl(new MarkAsOpen, array('invoice_id'=>$invoice->id)));
                $this->setConfirmTitle(trans('app.mark_as_open'));
                $this->setConfirmMessage(trans('app.mark_as_open_message', array('invoice'=>$invoice->name)));
            } else {
                $this->setTitle('');
                $this->setUrl('#');
            }
        } else {
            $this->setTitle('');
            $this->setUrl('#');
        }
        
		$this->setOnFinishAction(new ReloadCurrentWidget);
		
        return parent::execute();
    }
}