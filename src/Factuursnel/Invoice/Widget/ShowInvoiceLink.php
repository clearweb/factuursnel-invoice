<?php namespace Factuursnel\Invoice\Widget;

use Clearweb\Clearworks\Action\ActionAnchor;

use Factuursnel\Invoice\Page\ViewPage;

use Clearworks;

class ShowInvoiceLink extends ActionAnchor
{
    public function execute()
    {
        $this->setTitle(trans('app.view'));
        
        $this->setUrl(
                      Clearworks::getPageUrl(new ViewPage, array('invoice_id'=>$this->getParameter('id')))
                      )
            ;
        
        return parent::execute();
    }
}