<?php namespace Factuursnel\Invoice\Widget;

use Clearweb\Clearwebapps\Widget\ListWidget;

use Clearweb\Clearwebapps\Lists\ListObject;

use Factuursnel\Invoice\Invoice;

class InvoiceLineListWidget extends ListWidget
{
    public function init()
    {
        $invoice = Invoice::find($this->getParameter('invoice_id'));
        
        $items   = $invoice->invoice_lines()->getResults()->toArray();
        
        $vat        = 0;
        $totalExVat = 0;
        $total      = 0;
        
        foreach($items as $line) {
            $vat        += $line['total_vat'];
            $totalExVat += $line['total_price'];
            $total      += $line['total_price'] + $line['total_vat'];
        }
        
        /*
        $items[] = array(
                         'id' => '-1',
                         'amount' => '',
                         'unit_price' => '',
                         'name' => '',
                         'total_price' => '',
                         'vat_percentage' => '',
                         );
        */
        
        $items[] = array(
                         'id' => '-1',
                         'amount' => '<b>'.trans('app.total_ex_vat').'</b>',
                         'unit_price' => '',
                         'name' => '',
                         'total_price' => '<b>'.$totalExVat.'</b>',
                         'vat_percentage' => '',
                         );
        $items[] = array(
                         'id' => '-1',
                         'amount' => '<b>'.trans('app.total_vat').'</b>',
                         'unit_price' => '',
                         'name' => '',
                         'total_price' => '<b>'.$vat.'</b>',
                         'vat_percentage' => '',
                         );
        $items[] = array(
                         'id' => '-1',
                         'amount' => '<b style="font-size:18px;">'.trans('app.total').'</b>',
                         'unit_price' => '',
                         'name' => '',
                         'total_price' => '<b style="font-size:18px;">'.$total.'</b>',
                         'vat_percentage' => '',
                         );
        
        $list = with(new ListObject)
            ->addColumn('description', function($row){return $row['name'];})
            ->addColumn('unit_price', function($row){
                    $html = '<div style="text-align:right">';
                    if (is_numeric($row['unit_price']))
                        $html .= '&euro; '.number_format($row['unit_price'], 2, ',', '.');
                    else
                        $html .= $row['unit_price'];
                    
                    $html .= '</div>';
                    
                    return $html;
                })
            ->addColumn('amount', function($row) {
                    $html  = '<div style="text-align:right">';
                    
                    $html .= $row['amount'];
                    
                    $html .= '</div>';
                    
                    return $html;
                })
            ->addColumn('total_price', function($row) {
                    $html = '<div style="text-align:right">';
                    
                    if(preg_match('#^(<b.*>)?([^0-9.]*)([0-9.]+)([^0-9.]*)$#', $row['total_price'], $matches)) {
                        $html .= $matches[1];
                        $html .= $matches[2];
                        $html .= '&euro;&nbsp;'.number_format($matches[3], 2, ',', '.');
                        $html .= $matches[4];
                        //$html .= $matches[5];
                    } else {
                        $html .= $row['total_price'];
                    }
                    
                    $html .= '</div>';
                    
                    return $html;
                })
            ->addColumn('vat_percentage', function($row) {
                    $html  = '<div style="text-align:right">';
                    
                    if (is_numeric($row['vat_percentage']))
                        $html .= $row['vat_percentage'].' %';
                    
                    $html .= '</div>';
                    
                    return $html;
                })
            ->setItems($items)
            ;
        
        $this->setList($list)
            ->setShowNewButton(false)
            ->setName('invoice-lines')
            ->addClass('invoice-lines')
            ;
        
        return parent::init();
    }
}