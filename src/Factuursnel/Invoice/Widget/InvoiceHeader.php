<?php namespace Factuursnel\Invoice\Widget;

use Clearweb\Clearworks\Widget\CommunicatingWidget;
use Clearweb\Clearworks\Content\RawHTML;

use Factuursnel\Invoice\Invoice;

use FileManager;
use Settings;
use View;

class InvoiceHeader extends CommunicatingWidget
{
    public function init()
    {
        $this->setName('invoice-header')
            ->addStyle('/invoices/css/invoice.css')
            ;
        
        parent::init();
        
        $invoice = Invoice::find($this->getParameter('invoice_id', 0));
        
        $logoFile = FileManager::getFile(Settings::get('logo'));
        
        $this->getContainer()
            ->addViewable(with(new RawHTML)->setHTML(
                                                     View::make('invoice::invoice_header')
                                                     ->with('invoice', $invoice)
                                                     ->with('logoUrl', $logoFile->getUrl())
                                                     ))
            ->addClass('invoice-header')
            ;
        
        return $this;
    }
}