<?php namespace Factuursnel\Invoice\Widget;

use Session;

class DraftInvoiceLineList extends \Clearweb\Clearwebapps\Widget\ListWidget
{
	public function init()
	{
		$this->setName('draft-invoice-lines')
			->setTitle(ucfirst(trans_choice('app.invoice_line', 2)))
			;
        
		parent::init();
		
		$this->setShowNewButton(true)
			->addListener('invoice_line')
			;
		
		$this->getList()
			->addColumn('description')
			->addColumn(
						'unit_price',
						function(array $row) {
							return '&euro;&nbsp;'.number_format($row['unit_price'], 2, ',', '.');
						}
						)
			->addColumn('amount')
			->addColumn(
						'total_price',
						function(array $row) {
							return '&euro;&nbsp;'.number_format($row['total_price'], 2, ',', '.');
						}
						)
			->addColumn(
						'vat_percentage',
						function(array $row) {
							return $row['vat_percentage'].'&nbsp;%';
						}
						)
            
			->setListItemName('invoice_line')
			->setSelected($this->getParameter('invoice_line'))
            
            ->addActionLink(
                            (new DraftInvoiceLineDeleteLink)
                            ->setDeleteAction(new DraftInvoiceLineDelete)
                            ->setModelClass('\Factuursnel\Invoice\InvoiceLine')
                            )
			;
		
		
		
		return $this;
	}
	
	public function execute()
	{
		$this->getList()
			->setItems(Session::get('draft_invoice_lines', array()))
			;
		
		return parent::execute();
	}
}