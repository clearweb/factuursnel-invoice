<?php namespace Factuursnel\Invoice\Widget;

use Clearweb\Clearworks\Action\Action;

use Factuursnel\Invoice\Invoice;

class MarkAsClosed extends Action
{
	public function execute()
	{
		$invoice = Invoice::find($this->getParameter('invoice_id', 0));
		
		$invoice->setStatus(Invoice::STATUS_PAID);
		$invoice->save();

		return $this;
	}
	
	public function getJSON()
	{
		return 'success';
	}
}