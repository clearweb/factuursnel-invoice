<?php namespace Factuursnel\Invoice\Widget;

use Clearweb\Clearwebapps\Eloquent\DeleteActionLink;

class DraftInvoiceLineDeleteLink extends DeleteActionLink
{
    function getConfirmMessage() {
		return trans('invoice::invoice.delete-confirm');
	}
    
    function execute()
    {
        $this->setParameter('id', null);
        return parent::execute();
    }
}