<?php namespace Factuursnel\Invoice\Widget;

use Clearweb\Clearworks\Widget\CommunicatingWidget;
use Clearweb\Clearworks\Content\RawHTML;

use Factuursnel\Invoice\Invoice;

use FileManager;
use Settings;
use View;

class InvoiceFooter extends CommunicatingWidget
{
    public function init()
    {
        $this->setName('invoice-footer')
            ->addStyle('packages/factuursnel/invoice/css/invoice.css')
            ;
        
        parent::init();
        
        $invoice = Invoice::find($this->getParameter('invoice_id', 0));
        
        $this->getContainer()
            ->addViewable(with(new RawHTML)->setHTML(
                                                     View::make('invoice::invoice_footer')
                                                     ->with('invoice', $invoice)
                                                     ))
            ->addClass('invoice-footer')
            ;
        
        return $this;
    }
}