<?php namespace Factuursnel\Invoice\Widget;

use Clearweb\Clearworks\Action\Action;
use Session;

class DraftInvoiceLineDelete extends Action
{
    public function execute()
    {
        $invoiceLines = Session::get('draft_invoice_lines', array());
        
        unset($invoiceLines[$this->getParameter('invoice_line', 1) - 1]);
        
        Session::set('draft_invoice_lines', $invoiceLines);
    }
    
    public function getJSON()
    {
        return json_encode('success');
    }
}