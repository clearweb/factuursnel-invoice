<?php namespace Factuursnel\Invoice\Widget;

use Factuursnel\Invoice\Invoice;

use Factuursnel\Client\Client;

use Illuminate\Database\Eloquent\Builder;

class ListWidget extends \Clearweb\Clearwebapps\Eloquent\PagableListWidget
{
    public function getModelClass()
    {
        return '\Factuursnel\Invoice\Invoice';
    }
    
    public function init()
    {
        parent::init();
        
        $this->addAction('open-invoices', new OpenFilter);
        
        $this->setShowNewButton(false)
            ->setLangPrefix('invoice::invoice')
            ->setOrderAttribute('date')
            ->setOrderDirection('desc')
            ->addQueryListener('client_id')
            ->addListener('only-open')
            
            ->getList()
            ->addColumn('price', function(array $row) {
                    $invoice = Invoice::find($row['id']);
                    
                    $totalPrice = 0;
                    
                    if ( ! empty($invoice)) {
                        $totalPrice = $invoice->getTotalPriceWithVat();
                    }
                    
                    return '<div style="text-align:right">&euro; '.number_format($totalPrice, 2, ',', '.').'</div>';
                },
                3
                )
            ->moveColumn('number', 2)
			->replaceColumn('status', function($row) {
                $invoice = Invoice::find($row['id']);
                
                $totalPrice = 0;
                
                if ( ! empty($invoice)) {
                    $totalPrice = $invoice->getTotalPrice();
                }
                
                if ($totalPrice > 0) {
                    return trans('invoice_status.'.$row['status']);
                } else {
                    return trans('invoice_status.credit');
                }
			})
            ->removeColumn('description')
            
			->addActionLink(new ChangeStatusLink)
            ->addActionLink(new ShowInvoiceLink)
            ;
        
        $client = Client::find($this->getParameter('client_id', 0));
        
        if ( ! empty($client)) {
            $this->getList()
                ->removeColumn('client')
                ;
        }
        
        return $this;
    }
    
    protected function applyQueryFilters(Builder $builder) {
        if ($this->getParameter('only-open', 0) == 1) {
            $builder->where('status', '!=', Invoice::STATUS_PAID);
        }
        
        return parent::applyQueryFilters($builder);
    }
    
    protected function applyOrdering(Builder $builder)
    {
        $builder = parent::applyOrdering($builder);
        $builder->orderBy('number', 'desc');
        
        return $builder;
    }
}