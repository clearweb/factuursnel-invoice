<?php namespace Factuursnel\Invoice\Widget;

use Factuursnel\Invoice\Invoice;

use Clearweb\Clearwebapps\Widget\FormWidget;

use Clearweb\Clearwebapps\Form\Form;

use Clearweb\Clearwebapps\Form\DateField;
use Clearweb\Clearwebapps\Form\SubmitField;

use Clearweb\Clearwebapps\Form\Validator;

use Clearweb\Clearwebapps\File\File;
use FileManager;

use Clearweb\Clearworks\Action\ActionAnchor;

use Factuursnel\Base\Vat\Vat;

class ExportInvoicesWidget extends FormWidget
{
    private $csvFile = null;
    
    public function init()
    {
        $this->setName('export-invoices')
             ->setForm(
                 (new Form)
                 ->addField(
                     (new DateField)
                     ->setName('date_from')
                     ->setLabel(trans('invoice::export.date_from'))
                 )
                 ->addField(
                     (new DateField)
                     ->setName('date_till')
                     ->setLabel(trans('invoice::export.date_till'))
                 )
                 ->addField(
                     (new SubmitField)
                     ->setName('export')
                     ->setLabel(trans('invoice::export.export'))
                 )
             )
             ->setValidator((new Validator)->setRules([
                 'date_from' => 'required|date',
                 'date_till' => 'required|date',
             ]))
             ->setTitle(trans('invoice::export.export'))
            ;
        return parent::init();
    }

    public function execute()
    {

        
        if ($this->getSubmitted()) {
            $file = $this->getCSVFile();
            
        }

        parent::execute();
        
        return $this;
    }

    public function submit(array $post)
    {
        $clientid = $this->getParameter('client_id', 0);
        $file = $this->writeExportFile($post['date_from'], $post['date_till'], $clientid);

        $this->setCSVFile($file);

        $link = (new ActionAnchor)->setURL($file->getUrl())->setTitle(trans('invoice::export.download_export'))->addClass('button')->addClass('btn');
        $this->addViewable($link);
        //$this->addAction('download', $link);
    }

    function writeExportFile($dateFrom, $dateTill, $clientId)
    {
        $fileName = "invoice_export_{$dateFrom}_{$dateTill}";
        
        if ($clientId) {
            $fileName .= "_$clientId";
        }
        
        $fileName .= '.csv';
        
        $file = FileManager::generateNewFile($fileName);

        
        $invoiceQuery = Invoice::whereBetween('date', [$dateFrom, $dateTill]);
        
        if($clientId) {
            $invoiceQuery = $invoiceQuery->where('client_id', $clientId);
        }
        
        $invoices = $invoiceQuery->get();
        
        $this->writeCSV($file, $invoices);
        
        return $file;
    }

    public function writeCSV(File $file, $invoices)
    {
        $csvFile  = fopen($file->getFullPath(), 'w');
        $vatTypes = Vat::all();
        
        $line = [
                 'number',
                 'client-number',
                 'client-name',
                 'date',
                 'description',
                 'total',
                 ];
        
        foreach($vatTypes as $vat) {
            $line[] = 'vat-'.$vat->percentage;
        }
        
        fputcsv($csvFile, $line);
        
        foreach($invoices as $invoice) {
            $line = [
                     $invoice->getNumber(),
                     $invoice->client->getNumber(),
                     $invoice->client->name,
                     $invoice->date,
                     $invoice->description,
                     $invoice->getTotalPrice(),
                     ];
            
            foreach($vatTypes as $vat) {
                // get vat for vat type
                $vatAmount = 0;
                
                foreach($invoice->invoice_lines()->get() as $invoiceLine) {
                    if ($invoiceLine->vat_percentage == $vat->percentage) {
                        $vatAmount += $invoiceLine->total_vat;
                    }
                }
                
                $line[] = $vatAmount;
            }
            
            fputcsv($csvFile, $line);
        }
        
        fclose($csvFile);
    }

    public function setCSVFile(File $file)
    {
        $this->csvFile = $file;
        
        return $this;
    }

    public function getCSVFile()
    {
        return $this->csvFile;
    }
}