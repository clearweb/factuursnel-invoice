<?php namespace Factuursnel\Invoice\Widget;

use Clearweb\Clearwebapps\Form\Form;
use Clearweb\Clearwebapps\Form\TextField;
use Clearweb\Clearwebapps\Form\SelectField;
use Clearweb\Clearwebapps\Form\SubmitField;

use Clearweb\Clearwebapps\Form\Validator;

use Clearweb\Clearworks\Communication\ParameterChanger;

use Session;
use Factuursnel\Base\Vat\Vat;

class DraftInvoiceLineForm extends \Clearweb\Clearwebapps\Widget\FormWidget
{
	private $line_id = 0;
	
	public function init()
	{
		$this->setName('draft-invoice-line-form')
			->addListener('invoice_line')
            ->addStyle('packages/factuursnel/invoice/css/invoice-make.css')
			;
		
		$vatOptions = array();
		foreach(Vat::orderBy('rank')->get() as $vat) {
			$vatOptions[$vat->percentage] = $vat->name;
		}
		
		$lines = Session::get('draft_invoice_lines', array());
		$this->line_id = $this->getParameter('invoice_line', 0);
		
		if($this->line_id) {
			$line = $lines[$this->line_id - 1];
			$title = $line['amount'].'x '.$line['description'];
		} else {
			$title = trans('app.new_invoice_line');
		}
		
		$this->setForm(
					   with(new Form)
					   ->addField(with(new TextField)->setName('description')->setLabel(ucfirst(trans('app.description'))))
					   ->addField(with(new TextField)->setName('amount')->setLabel(ucfirst(trans('app.amount'))))
					   ->addField(with(new TextField)->setName('unit_price')->setLabel(ucfirst(trans('app.unit_price'))))
					   ->addField(with(new SelectField)->setName('vat_percentage')->setLabel(ucfirst(trans('app.vat_percentage')))->setOptions($vatOptions)->setSort(false))
					   ->addField(with(new SubmitField)->setName(trans('invoice::invoice.add-line')))
					   )
			->setValidator(
						   with(new Validator)
						   ->setRules(array())
						   )
			->setTitle($title)
			->addStyle('/invoices/css/make_invoice.css')
			->addClass('draft-invoice-line-form')
			;
		
		if ($this->line_id) {
			foreach($lines[$this->line_id - 1] as $key => $value) {
				if ($key != 'id') {
					try{
						$this->getForm()->getField($key)->setValue($value);
					} catch(\Exception $e) {}
				}
			}
 			$this->getForm()->setValues($lines[$this->line_id - 1]);
		}
		
		parent::init();
		return $this;
	}
	
	public function submit(array $post)
	{
		$lines = Session::get('draft_invoice_lines', array());
		
		$line = array_merge(
							$post,
							array(
								  'total_price' => (float) $post['amount'] * (float) $post['unit_price']
								  )
							);
		
		if ($this->line_id) {
			$line['id'] = $this->line_id;
			$lines[$this->line_id - 1] = $line;
		} else {
			$line['id'] = count($lines) + 1;
			$lines[] = $line;
			$this->line_id = count($lines);
		}
		
		Session::set('draft_invoice_lines', $lines);
	}
	
	public function execute()
	{
		parent::execute();
		
		if ($this->getSubmitted()) {
			$this->addViewable(with(new ParameterChanger)->setParameter('invoice_line', $this->line_id));
		}
		
		return $this;
	}
}