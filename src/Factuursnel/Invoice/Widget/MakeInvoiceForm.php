<?php namespace Factuursnel\Invoice\Widget;

use Clearweb\Clearworks\Action\Redirect;

use Clearweb\Clearwebapps\Widget\FormWidget;

use Clearweb\Clearwebapps\Form\Form;
use Clearweb\Clearwebapps\Form\Validator;


use Clearweb\Clearwebapps\Form\HiddenField;
use Clearweb\Clearwebapps\Form\DateField;
use Clearweb\Clearwebapps\Form\TextField;
use Clearweb\Clearwebapps\Form\TextArea;
use Clearweb\Clearwebapps\Form\SelectField;
use Clearweb\Clearwebapps\Form\SubmitField;

use Factuursnel\Invoice\Page\ViewPage;

use Order\Order;
use Factuursnel\Invoice\Invoice;
use Factuursnel\Invoice\InvoiceLine;

use Clearworks;
use Settings;
use Session;

class MakeInvoiceForm extends FormWidget
{
    private $invoiceId;
    
    public function init()
    {
        $this->setName('make-invoice')
            ->setTitle(trans('invoice::invoice.details'))
            ;
        
        $form = new Form;
		
        $form
            ->addField(
                       with(new HiddenField)
                       ->setName('nothing')
                       ->setValue('dummy')
                       )
            ->addField(
                       with(new TextField)
                       ->setName('subject')
                       ->setLabel(ucfirst(trans('invoice::invoice.subject')))
                       )
            ->addField(
                       with(new TextArea)
                       ->setName('description')
                       ->setLabel(ucfirst(trans('invoice::invoice.description')))
                       )
            ->addField(
                       with(new DateField)
                       ->setName('date')
                       ->setLabel(ucfirst(trans('invoice::invoice.date')))
                       ->setValue(date('Y-m-d'))
                       )
            ->addField(
                       with(new SubmitField)
                       ->setName(ucfirst(trans('app.save')))
                       )
            ;
        
        $validator = new Validator;
        
        $validator->setRules(
                             [
                              'date' => 'required|date',
                              'subject' => 'required',
                              //'description' => 'required',
                              ]
                             );
        
        $this->setForm($form);
        $this->setValidator($validator);
        
        
        return parent::init();
    }
    
    public function submit(array $post)
    {
        $invoiceLines = Session::get('draft_invoice_lines', array());
        $invoice = new Invoice;
        
        $invoice->client_id   = $this->getParameter('client_id');
        $invoice->status      = Invoice::STATUS_OPEN;
        $invoice->date        = $post['date'];
        $invoice->subject     = $post['subject'];
        $invoice->description = $post['description'];
        
        $invoice->due_date    = date('Y-m-d', strtotime($post['date']) + (Settings::get('invoice_due_period', 30) * 86400));
        
        $index                = Settings::get('invoice_index', 1);
        $invoice->number      = Settings::get('invoice_prefix', 'IN').str_pad($index, 5, '0', STR_PAD_LEFT);
        
        $invoice->save();
        
        Settings::set('invoice_index', Settings::get('invoice_index', 1) + 1);
		
        foreach($invoiceLines as $rawInvoiceLine) {
            $invoiceLine = new InvoiceLine;
            $invoiceLine->invoice_id     = $invoice->id;
            $invoiceLine->name           = $rawInvoiceLine['description'];
            $invoiceLine->amount         = $rawInvoiceLine['amount'];
            $invoiceLine->unit_price     = $rawInvoiceLine['unit_price'];
            $invoiceLine->vat_percentage = $rawInvoiceLine['vat_percentage'];
            $invoiceLine->total_price    = $rawInvoiceLine['amount'] * $rawInvoiceLine['unit_price'];
            $invoiceLine->total_vat      = ($invoiceLine->total_price * $invoiceLine->vat_percentage) / 100;
            
            $invoiceLine->save();
        }
		
        $this->invoiceId = $invoice->id;

		Session::set('draft_invoice_lines', array());
		
        return $invoice->id;
    }

    public function execute()
    {
        parent::execute();
        
        if ($this->getSubmitted()) {
            $this->getContainer()
                ->addViewable(
                              with(new Redirect)
                              ->setLocation(
                                            Clearworks::getPageUrl(new ViewPage, array('invoice_id'=>$this->invoiceId))
                                            )
                              )
                ;
        }
        
        return $this;
    }
}