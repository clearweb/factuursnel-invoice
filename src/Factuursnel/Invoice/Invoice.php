<?php namespace Factuursnel\Invoice;

use Eloquent;

class Invoice extends Eloquent
{
    protected $table = 'invoice';

    const STATUS_OPEN = 'open';
    const STATUS_PAID = 'paid';
    
    public function getNumber()
    {
        return $this->number;
    }
	
	public function getStatus()
	{
		return $this->status;
	}
	
	public function setStatus($status)
	{
		$this->status = $status;
		
		return $this;
	}
    
    public function getTotalPrice()
    {
        $totalPrice = array_reduce(
                                   $this->invoice_lines()->get()->all(),
                                   function($sum, $line) {
                                       $sum += $line->total_price;
                                       return $sum;
                                   },
                                   0
                                   )
            ;
        
        return $totalPrice;
    }
    
    public function getTotalPriceWithVat()
    {
        $totalPrice = array_reduce(
                                   $this->invoice_lines()->get()->all(),
                                   function($sum, $line) {
                                       $sum += $line->total_price + $line->total_vat;
                                       return $sum;
                                   },
                                   0
                                   )
            ;
        
        return $totalPrice;
    }
    
    public function invoice_lines()
    {
        return $this->hasMany('\Factuursnel\Invoice\InvoiceLine');
    }

    public function client()
    {
        return $this->belongsTo('\Factuursnel\Client\Client');
    }
    

}