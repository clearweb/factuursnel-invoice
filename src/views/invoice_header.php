<div class="company-info">
  <div class="company-logo"><img src="<?php echo $logoUrl; ?>" /></div>
  <div class="company-name"><?php echo Settings::get('company'); ?></div>
  <div class="company-address-1"><?php echo Settings::get('street'); ?> <?php echo Settings::get('house_number'); ?></div>
  <div class="company-address-2"><?php echo Settings::get('postal_code'); ?>, <?php echo Settings::get('city'); ?></div>
  <?php if (Settings::get('phone')): ?>
  <div class="company-phone">Tel: <?php echo Settings::get('phone'); ?></div>
  <?php endif; ?>
  <?php if (Settings::get('fax')): ?>
  <div class="company-fax">Fax: <?php echo Settings::get('fax'); ?></div>
  <?php endif; ?>
  <div class="company-iban">IBAN: <?php echo Settings::get('iban'); ?></div>
  <div class="company-coc">KvK: <?php echo Settings::get('coc_number'); ?></div>
  <div class="company-vat">BTW: <?php echo Settings::get('vat_number'); ?></div>
</div>

<div class="client-details">
  <div class="client-name"><?php echo $invoice->client->name; ?></div>
  <div class="client-address-1"><?php echo $invoice->client->address->street; ?> <?php echo $invoice->client->address->number; ?> <?php echo $invoice->client->address->extension; ?></div>
  <div class="client-address-2"><?php echo $invoice->client->address->postal_code; ?>, <?php echo $invoice->client->address->city; ?></div>
</div>

<h2 class="invoice-title"><?php if ($invoice->getTotalPrice() > 0): ?>Factuur<?php else: ?>Creditfactuur<?php endif; ?></h2>
<div class="invoice-description"><?= $invoice->description ?></div>
<div class="invoice-details">
  <table>
    <tr>
      <td>Factuurnr:<td><td><?php echo $invoice->getNumber(); ?><td>
    </tr>
    <tr>
      <td>Klantnr:<td><td><?php echo $invoice->client->getNumber(); ?><td>
    </tr>
    <tr>
      <td>Datum:<td><td><?php echo date('d-m-Y', strtotime($invoice->date)); ?><td>
    </tr>
  </table>
</div>
