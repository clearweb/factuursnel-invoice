<?php
if (Settings::get('invoice_footer_text', '') != '') {
    $text = Settings::get('invoice_footer_text', '');
    echo str_replace(':due-date', date('d-m-Y', strtotime($invoice->due_date)), $text);
}
?>