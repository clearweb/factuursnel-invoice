<?php
return [
    'export_invoices' => 'Facturen exporteren',
    'export_invoices_for' => 'Facturen exporteren voor :client',

    'date_from' => 'Vanaf datum',
    'date_till' => 'Tot datum',

    'export' => 'Exporteren',
    'download_export' => 'Exportbestand downloaden',
];