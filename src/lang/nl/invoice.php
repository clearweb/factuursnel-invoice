<?php

return [
        /* properties */
        'subject' => 'Onderwerp',
        'description' => 'Beschrijving',
        'date' => 'Factuurdatum',
        'client'  => 'klant|klanten',
        'number'=>'nummer',
        'price' => 'prijs',
        'status' => 'status',
        'due_date' => 'Verloopdatum',
        
        'delete-confirm' => 'Weet u zeker dat u de factuurregel wil verwijderen?',
        'show-only-open' => 'Alleen openstaand',
        
        'details' => 'Factuuurdetails',
        
        'add-line' => 'regel toevoegen',
        ];